﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class SceneSwitcher : MonoBehaviour
{
    public Button playButton;
    public Button quitButton;

    private void Start()
    {
        this.playButton.onClick.AddListener(changeScene);
        this.quitButton.onClick.AddListener(quitGame);
    }

    private void changeScene()
    {
        SceneManager.LoadScene("Level_1");
    }

    private void quitGame()
    {
        Application.Quit();
    }
}
