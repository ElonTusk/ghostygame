﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class ReturnToMenu : MonoBehaviour
{

    public Button returnButton;
    
    void Start()
    {
        this.returnButton.onClick.AddListener(goToMenu);
    }

    public void goToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
