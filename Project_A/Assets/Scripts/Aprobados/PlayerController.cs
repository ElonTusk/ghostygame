﻿using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
   public Rigidbody2D playerRb;
   public Animator anim;
   public Joystick joystick;
   public Button jumpButton;
   public ParticleSystem buffParticles;
   public TMPro.TMP_Text buffText;
   public float movementSpeed = 2f;
   public float jumpForce = 5f;
   public float fallMultiplier = 2.5f;

   private Collider2D _platformCollider;
   private GameObject[] _ghostPlatforms;
   private bool _isJumping;
   private static readonly int Running = Animator.StringToHash("Running");
   

   private void Start()
   {
      _ghostPlatforms = GameObject.FindGameObjectsWithTag("GhostWall");
   }

   private void FixedUpdate()
   {
      if (joystick.Horizontal > 0)
      {
         playerRb.velocity = new Vector2(movementSpeed, playerRb.velocity.y);
         anim.SetBool(Running, true);
         transform.localScale = new Vector3(1, 1, 1);
      }
      else if (joystick.Horizontal < 0)
      {
         playerRb.velocity = new Vector2(-movementSpeed, playerRb.velocity.y);
         anim.SetBool(Running, true);
         transform.localScale = new Vector3(-1, 1, 1);
      }
      
      if (Mathf.Approximately(joystick.Horizontal, 0f))
      {
         anim.SetBool(Running, false);
      }

      if (playerRb.velocity.y < 0 || (playerRb.velocity.y > 0 && _isJumping))
      {
         playerRb.velocity += Time.deltaTime * Physics2D.gravity.y * (fallMultiplier - 1) * Vector2.up;
      }

      jumpButton.onClick.AddListener(Jump);
   }

   private void Jump()
   {
      if (!_isJumping)
      {
         playerRb.velocity = new Vector2(playerRb.velocity.x, jumpForce);
         _isJumping = true;
      }
   }

   private void OnCollisionEnter2D (Collision2D col)
   {

      switch (col.gameObject.tag)
      {
         case "Ground":
            _isJumping = false;
            break;
         case "speedBuff":
            buffText.text = "Speed Buff Active";
            SpawnParticles(col.transform);
            Destroy(col.gameObject);
            movementSpeed = 3.5f;
            ChangeColor(col.gameObject.GetComponent<SpriteRenderer>().color);
            break;
         case "jumpBuff":
            buffText.text = "Jump Buff Active";
            SpawnParticles(col.transform);
            Destroy(col.gameObject);
            jumpForce = 10f;
            ChangeColor(col.gameObject.GetComponent<SpriteRenderer>().color);
            break;
         case "ghostBuff":
            buffText.text = "Ghost Buff Active";
            SpawnParticles(col.transform);
            Destroy(col.gameObject);
            ChangeColor(col.gameObject.GetComponent<SpriteRenderer>().color);
            foreach (GameObject ghostPlatform in _ghostPlatforms)
            {
               ghostPlatform.GetComponent<Collider2D>().isTrigger = true;
            }
            break;
         case "endLevel":
            SceneManager.LoadScene("MainMenu");
            break;
      }
   }

   private void ChangeColor(Color color)
   {
      gameObject.GetComponent<SpriteRenderer>().color = color;
      gameObject.GetComponentInChildren<Light2D>().color = color;
   }

   private void SpawnParticles(Transform buffPos)
   {
      var buffParticlesMain = buffParticles.main;
      buffParticlesMain.startColor = buffPos.GetComponent<SpriteRenderer>().color;
      Instantiate(buffParticles, buffPos.position, Quaternion.identity);
   }
}
